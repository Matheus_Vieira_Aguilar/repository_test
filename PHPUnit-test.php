<?php
	
require 'GumballMachine.php';

class GumballMachineTest extends PHPUnit\Framework\TestCase
{
	public $gumballMachineInstance;

	public function setup()
	{
		$this->gumballMachineInstance = new GumballMachine();
	}

	public function testIfWheelWorks()
	{
		$this->gumballMachineInstance->setGumballs(100);
		$this->gumballMachineInstance->turnWheel();
		$this->assertEquals(98, $this->gumballMachineInstance->getGumballs());
	}
}